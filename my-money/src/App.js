import React from 'react'
import Rest from './utils/rest'
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Header from './elements/Header'
import Home from './pages/Home'
import Login from './pages/Login'
import Movimentacoes from './pages/Movimentacoes'

function App() {
  return (
    <Router>
      <div>
        <Header />
        <Switch>
          <Route path="/login" exact component={Login} />
          <Route path="/" exact component={Home} />
          <Route path="/movimentacoes/:data" exact component={Movimentacoes} />
        </Switch>
      </div>
    </Router>
  );
}
export default App
