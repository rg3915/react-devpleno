import React, { useState, useEffect } from 'react'
import { usePost } from '../utils/rest'
import { Redirect } from 'react-router-dom'

const url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyB8p2iqKONvC1yha3LZYDibH26weFQ1lfg'

const Login = () => {
  const [postData, signin] = usePost(url)
  const [logado, setLogado] = useState(false)
  const [email, setEmail] = useState('')
  const [senha, setSenha] = useState('')

  useEffect(() => {
    if (Object.keys(postData.data).length > 0) {
      // logou
      localStorage.setItem('token', postData.data.idToken)
      window.location.reload()
    }
  }, [postData])

  useEffect(() => {
    const token = localStorage.getItem('token')
    if (token) {
      setLogado(true)
    }
  })

  const login = async () => {
    await signin({
      email,
      password: senha,
      returnSecureToken: true
    })
  }

  const onChangeEmail = (e) => {
    setEmail(e.target.value)
  }

  const onChangeSenha = (e) => {
    setSenha(e.target.value)
  }

  if (logado) {
    return <Redirect to="/" />
  }

  return (
    <div className="text-center">
      <div className="form-signin">
        <h1 className="h3 mb-3 font-weight-normal">Login</h1>
        {
          postData.error && postData.error.length > 0 &&
          <div class="alert alert-danger" role="alert">
            E-mail e/ou senha inválidos.
          </div>
        }
        <input
          className="form-control" 
          type="text" 
          value={email} 
          onChange={onChangeEmail} 
          placeholder="Seu e-mail..."
        />
        <input
          className="form-control" 
          type="password" 
          value={senha} 
          onChange={onChangeSenha} 
          placeholder="Sua senha..."
        />
        <button className="btn btn-lg btn-primary btn-block" onClick={login}>Login</button>
      </div>
    </div>
  )
}
export default Login