import firebase from 'firebase/app'
import 'firebase/database'

const firebaseConfig = {
  apiKey: "AIzaSyBeH0BK5JsL6bPy4J4YnCTJa2kl7f7h3sc",
  authDomain: "comentaki-app42.firebaseapp.com",
  databaseURL: "https://comentaki-app42.firebaseio.com",
  projectId: "comentaki-app42",
  storageBucket: "comentaki-app42.appspot.com",
  messagingSenderId: "25758283190",
  appId: "1:25758283190:web:3c5be95b01a3d4028abd17"
};
firebase.initializeApp(firebaseConfig)

export default firebase