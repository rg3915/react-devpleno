import React, { useState, useEffect } from 'react'
import './App.css';

import firebase from './firebase'

const useDatabase = endpoint => {
  const [data, setData] = useState({})

  useEffect(() => {
    const ref = firebase.database().ref(endpoint)
    ref.on('value', snapshot => {
      setData(snapshot.val())
    })
    return () => {
      ref.off()  // desconecta do Firebase
    }
  }, [endpoint])
  return data
}

const useDatabasePush = endpoint => {
  const [status, setStatus] = useState('')
  
  const save = data => {
    const ref = firebase.database().ref(endpoint)
    ref.push(data, err => {
      if (err) {
        setStatus('ERROR')
      } else {
        setStatus('SUCCESS')
      }
    })
  }
  return [status, save]
}

const Comments = ({ visible }) => {
  const endpoint = visible ? 'test' : 'test/a'
  const data = useDatabase(endpoint)
  return (
    <pre>{JSON.stringify(data, null, 2)}</pre>
  )
}

const Firstname = () => {
  const data = useDatabase('test/firstname')
  return (<pre>{JSON.stringify(data, null, 2)}</pre>)
}

function App() {
  const [visible, toggle] = useState(true)
  const [status, save] = useDatabasePush('test')

  return (
    <div>
      <button onClick={() => toggle(!visible)}>Toggle</button> <br/>
      Status: {status} <br/>
      <button onClick={() => save({ firstname: 'Abel', lastname: 'Silva' })}>Salvar</button>
      <Comments visible={visible} />
      <Firstname />
    </div>
  );
}

export default App;
