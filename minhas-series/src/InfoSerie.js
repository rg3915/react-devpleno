import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import { Badge } from 'reactstrap'

const InfoSerie = ({ match }) => {
  const [form, setForm] = useState({
    name: '',
    comments: ''
  })
  const [success, setSuccess] = useState(false)
  const [mode, setMode] = useState('INFO')

  const [data, setData] = useState({})
  useEffect(() => {
    axios.get('/api/series/' + match.params.id)
      .then(res => {
        setData(res.data)
        setForm(res.data)
      })
  }, [match.params.id])

  const [genres, setGenres] = useState([])
  const [genreId, setGenreId] = useState('')
  useEffect(() => {
    axios.get('/api/genres')
      .then(res => {
        setGenres(res.data.data)
        const genres = res.data.data
        const encontrado = genres.find(value => data.genre === value.name)
        if (encontrado && form) {
          setGenreId(encontrado.id)
        }
      })
  }, [data])

  // custom header
  const masterHeader = {
    height: '50vh',
    minHeight: '500px',
    backgroundImage: `url('${data.background}')`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
  }

  const onChange = field => e => {
    setForm({
      ...form,
      [field]: e.target.value
    })
  }

  const onChangeGenre = e => {
    setGenreId(e.target.value)
  }

  const seleciona = value => () => {
    setForm({
      ...form,
      status: value
    })
  }

  const save = () => {
    axios.put('/api/series/' + match.params.id, { ...form, genre_id: genreId })
      .then(res => {
        setSuccess(true)
      })
  }

  if (success) {
    return <Redirect to='/series' />
  }

  return (
    <div>
      <header style={masterHeader}>
        <div className='h-100' style={{ background: 'rgba(0,0,0,0.7)' }}>

          <div className='h-100 container'>
            <div className="row h-100 align-items-center">

              <div className="col-3">
                <img className="img-fluid img-thumbnail" src={data.poster} alt={data.name} />
              </div>

              <div className="col-9">
                <h1 className="font-weight-light text-white">{data.name}</h1>
                <div className="lead text-white">
                  { data.status === 'ASSISTIDO' && <Badge color="success">Assistido</Badge> }
                  { data.status === 'PARA_ASSISTIR' && <Badge color="warning">Para assistir</Badge> }
                  <span className="pl-2">Gênero: {data.genre}</span> <br/>
                  {data.comments}
                </div>
              </div>

            </div>
          </div>
          
        </div>
      </header>
      <div className='container'>
        <button className='btn btn-primary mt-2' onClick={() => setMode('EDIT')}>Editar</button>
      </div>
      {
        // Truque pra retornar o conteúdo somente se mode for verdadeiro.
        mode === 'EDIT' &&
        <div className='container'>
          <h1>Editar série</h1>
          <button className='btn btn-secondary' onClick={() => setMode('INFO')}>Cancelar edição</button>
          <form>
            <div className='form-group'>
              <label htmlFor='name'>Nome</label>
              <input id='name' className='form-control' type='text' value={form.name} onChange={onChange('name')} placeholder='Nome da série' />
            </div>

            <div className='form-group'>
              <label htmlFor='comments'>Comentários</label>
              <input id='comments' className='form-control' type='text' value={form.comments} onChange={onChange('comments')} placeholder='Escreva um comentário' />
            </div>

            <div className='form-group'>
              <label htmlFor='genres'>Gênero</label>
              <select 
                id='genre' 
                className='form-control' 
                value={genreId}
                onChange={onChangeGenre}
              >
                {genres.map(genre => 
                  <option key={genre.id} value={genre.id}>{genre.name}</option>)
                }
              </select>
            </div>

            <div className="form-check">
              <input 
                id='assistido' 
                className='form-check-input' 
                type='radio' 
                checked={form.status === 'ASSISTIDO'}
                name='status' 
                value="ASSISTIDO" 
                onChange={seleciona('ASSISTIDO')}
              />
              <label htmlFor="assistido" className='form-check-label'>Assistido</label>
            </div>
            <div className="form-check">
              <input 
                id='paraAssistir' 
                className='form-check-input' 
                type='radio' 
                checked={form.status === 'PARA_ASSISTIR'}
                name='status' 
                value="PARA_ASSISTIR" 
                onChange={seleciona('PARA_ASSISTIR')}
              />
              <label htmlFor="paraAssistir" className='form-check-label'>Para assistir</label>
            </div>

            <button type='button'onClick={save} className='btn btn-primary'>Salvar</button>
          </form>
        </div>
      }
    </div>
  )
}

export default InfoSerie