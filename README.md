## Conceitos

* Redux
* ContextAPI
* HOCs
* RenderProps

## Projeto: Contador de Voltas

Reforçando o conceito de props

```
create-react-app 01-contador-voltas
cd 01-contador-voltas/src
rm -f  App.css App.js App.test.js index.css logo.svg
```

Em `index.js` remova a linha:

```
import './index.css';
```


```js
cat << EOF > App.js
import React from 'react';

function App() {
  return (
    <div className="App">
    </div>
  );
}

export default App;
EOF
```

### Começando o novo projeto

### Aula 02: Componentes e Props

### Aula 03: Utilizando o useState e useEffect

```js
  useEffect(() => {
    setInterval(() => {
      console.log('chamou');
    }, 1000)
  }, [])  // só será chamado uma vez.
```



### Aula 04: mais opções para useEffect

### Aula 05: finalizando o Contador de voltas

Criando componentes

`touch MostraVoltas.js MostraTempo.js Button.js`

```css
cat << EOF > styles.css
@import url('//fonts.googleapis.com/css?family=Raleway&display=swap');
EOF
```



**Resumo:** hooks é uma forma de manter dados e manipular a execução de instruções em componentes funcionais (lembrando que componentes funcionais somente retornam os elementos que serão mostrados/renderizados na tela) e apenas com javascript essas funções não armazenariam valores.

Hooks sempre começam com "use", como useState e useEffects.

**useState:** tira uma "foto" de um dado para o componente funcional, e retorna uma função capaz de alteração esta foto. Toda vez que esta foto é alterada, o componente é renderizado novamente. Esta foto também em react é conhecida como estado interno do componente ou state.


**useHook:** permite ter efeitos colaterais que serão executados na renderização de um componente. Se utilizado sem dependências (ex: useEffect(funcao)), será executado em toda renderização. Se utilizado com dependências vazias (ex: useEffect(funcao, [])), será executado somente na primeira vez que for renderizado (mesmo comportamento de um construtor). E caso seja passado um valor (ex: useEffect(funcao, [a])), será executado somente se a tiver seu valor alterado.


## Projeto: Minhas séries

https://github.com/tuliofaria/minhas-series-server

```
create-react-app minhas-series
cd minhas-series
rm -f App.css index.css App.test.js logo.svg
```

```js
// App.js
import React from 'react';

function App() {
  return (
    <div className="App">
      <h1>Minhas Séries</h1>
    </div>
  );
}

export default App;
```

### Instalando Bootstrap e reactstrap

```
npm install bootstrap
npm install reactstrap
```

https://reactstrap.github.io/


### Criando componentes

```
touch Header.js
```


### Instalando react-router-dom


```
npm install react-router-dom
```

### Instalando axios

```
npm install axios
```

### Rodando um servidor próprio

```
cd minhas-series
npm install https://github.com/tuliofaria/minhas-series-server
# rodando o server (num outro terminal)
node node_modules/minhas-series-server/index.js
```

Edite `package.json`

```
...
"proxy": "http://localhost:3002/"
```

### Separando os componentes

```
touch {Generos,NovoGenero,EditarGenero}.js
```


### Novo componente séries

```
touch {Series,NovaSerie}.js
cp NovaSerie.js InfoSerie.js
```

`onChange` com um parametro sendo passado numa função interna.

```js
# InfoSerie.js
  const onChange = field => e => {
    setForm({
      ...form,
      [field]: e.target.value
    })
  }

  ...
  <input id='name' className='form-control' type='text' value={form.name} onChange={onChange('name')} placeholder='Nome da série' />
```

## JS Básico

```
create-react-app js-basics
```


### Variáveis

```js
console.log(variavel1); // undefined
// Hoisting
var variavel1 = 10
console.log(variavel1);

console.log(variavel1); // gera um erro
let variavel1 = 10
console.log(variavel1);
```


> `let` é mutável.

> `const` é imutável.

Use mais o `const` até onde der.



### Funções

```js
function soma(a, b) {
  return a + b
}

const funcSoma = soma
console.log(funcSoma(4, 5));


const soma = function soma(a, b) {
  // context > this
  return a + b
}
console.log(soma(1, 3));
```

### arrow-functions

```js
const soma = (a, b) => {
  return a + b
}
```

ou

```js
const soma = (a, b) => a + b
```

### Variáveis

const num = 3.14

const nome = 'John'
const mensagem = `Olá ${nome}` // template string


### Objetos

```js
const obj = {
  firstName: 'John',
  lastName: 'Doe'
}
console.log(obj.firstName, obj.lastName);
console.log(obj['firstName'], obj['lastName']);

const keys = Object.keys(obj)
console.log(keys[0]);
```

high-order functions

```js
keys.forEach(item => {
  console.log(item, obj[item]);
})
```


### Vetores

```js
const array = [1, 2, 3]
console.log(array[0]);
```

### high-order functions

```js
const keys = Object.keys(obj)
const values = keys.map(item => {
  return obj[item]
})
console.log(values);
```

```js
const App = () => {
  const names = ['John', 'Steve']
  return (
    <div>
      {names.map(name => <p>{name}</p>)}
    </div>
  )
}
```

OU

```js
const App = () => {
  const names = ['John', 'Steve']
  const namesElements = names.map(name => <p>{name}</p>)
  return (
    <div>
      { namesElements }
    </div>
  )
}
```

### Destructuring Assignament

**Ex. 1**

`const name = obj.name`

Como a constante e a chave tem o mesmo nome, no caso `name`, podemos simplificar escrevendo apenas

`const { name } = obj`

**Ex. 2**

Num outro exemplo

```js
const getName = person => {
  return person.name
}
console.log(getName(obj));
```

Ou podemos mudar para

```js
const getName = ({ name }) => {
  return name
}
```

**Ex. 3**

```js
const obj = {
  firstName: 'John',
  lastName: 'Doe',
  address: {
    city: 'New York'
  }
}
const keys = Object.keys(obj)

const [i1, i2] = keys
console.log('keys', keys);
console.log(i1, i2);
```

Que podemos relacionar com useState

`const [counter, setCounter] = useState(0)`


### import / export módulos

```js
# Header.js
import React from 'react';

const Header = () => <h1>Olá</h1>

export default Header
```

```js
# App.js
import Header from './Header';
```

Ou importando um componente específico de dentro do componente.

```js
# Header.js
import React from 'react';

const Header = () => <h1>Olá</h1>
export const Header2 = () => <h1>Olá</h1>  // <-- exporta direto

export default Header
```

```js
# App.js
import { Header2 } from './Header';
```

### Componentes funcionais vs componentes Classe ES6

```js
class App2 extends React.Component {
  constructor(props) {
    super(props)
    this.increment = this.increment.bind(this)
  }
  state = {
    contador: 1
  }
  increment = () => {
    this.setState({ contador: this.state.contador + 1 })
  }
  render() {
    return (
      <h1>Contador (App2): {this.contador}</h1>
      <button onClick={this.increment}>Increment</button>
    )
  }
}

const App1 = () => {
  const [contador, setContador] = useState(1)
  const increment = () => {
    setContador(contador + 1)
  }
  return (
      <h1>Contador (App1): {contador}</h1>
      <button onClick={increment}>Increment</button>
  )
}

function App() {
  return (
    <div className="App">
      <h1>App</h1>
      <App1 />
      <App2 />
    </div>
  )
}
```


## Projeto: Contador com Classe ES6

```
create-react-app counter
```

```js
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    contador: 1
  }

  increment = () => {
    this.setState({
      contador: this.state.contador + 1
    })
  }

  decrement = () => {
    this.setState({
      contador: this.state.contador - 1
    })
  }

  clear = () => {
    this.setState({
      contador: 0
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          Contador: {this.state.contador}
          <button onClick={this.increment}>+</button>
          <button onClick={this.decrement}>-</button>
          <button onClick={this.clear}>Clear</button>
        </header>
      </div>
    )
  }
}

export default App;
```

Mudando para

```js
  clear() {
    this.setState({
      contador: 0
    })
  }
```

Erro

`TypeError: Cannot read property 'setState' of undefined`

Então precisamos criar o `constructor`

```js
  constructor(props) {
    super(props)
    this.clear = this.clear.bind(this)
  }
```


## Projeto: My Money

Firebase: mymoney-rg3915

https://mymoney-rg3915.firebaseio.com/

```
create-react-app my-money
cd my-money
npm install axios react-router-dom
cd src
rm -f App* index.css logo.svg
```

Em `index.js` remova a linha:

```
import './index.css';
```


```js
cat << EOF > App.js
import React from 'react'

function App() {
  return (
    <div>
      <h1>My Money</h1>
    </div>
  );
}

export default App
EOF
```

### Testanto o Firebase com axios

```js
import React from 'react'
import axios from 'axios'

function App() {
  axios.get('https://mymoney-rg3915.firebaseio.com/valor.json')
    .then(res => {
      console.log(res.data);
    })
  axios.post('https://mymoney-rg3915.firebaseio.com/valor.json', {
    outrovalor: 42
  })
    .then(res => {
      console.log(res.data);
    })
  return (
    <div>
      <h1>My Money</h1>
    </div>
  );
}

export default App
```

```js
import React, { useState, useEffect } from 'react'
import axios from 'axios'

const url = 'https://mymoney-rg3915.firebaseio.com/movimentacoes/2020-10.json'

function App() {
  const [data, setData] = useState({})
  useEffect(() => {
    axios.get(url)
      .then(res => {
        setData(res.data)
      })
  }, [])
  return (
    <div>
      <h1>My Money</h1>
      { JSON.stringify(data) }
    </div>
  );
}

export default App
```

```js
import React, { useState, useEffect } from 'react'
import axios from 'axios'

const url = 'https://mymoney-rg3915.firebaseio.com/movimentacoes/2020-10.json'

function App() {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState({})

  useEffect(() => {
    axios.get(url)
      .then(res => {
        setData(res.data)
        setLoading(false)
      })
  }, [])

  return (
    <div>
      <h1>My Money</h1>
      { JSON.stringify(data) }
      { loading && <p>Loading...</p> }
    </div>
  );
}

export default App
```

Refatorando

```js
import React, { useState, useEffect } from 'react'
import axios from 'axios'

const url = 'https://mymoney-rg3915.firebaseio.com/movimentacoes/2020-10.json'

function App() {
  const [data, setData] = useState({
    loading: true,
    data: {}
  })

  useEffect(() => {
    axios.get(url)
      .then(res => {
        setData({
          loading: false,
          data: res.data
        })
      })
  }, [])

  return (
    <div>
      <h1>My Money</h1>
      { JSON.stringify(data) }
      { data.loading && <p>Loading...</p> }
    </div>
  );
}

export default App
```

### Reducer

```js
import React, { useState, useEffect, useReducer } from 'react'
import axios from 'axios'

const url = 'https://mymoney-rg3915.firebaseio.com/movimentacoes/2020-10.json'

const reducer = (state, action) => {
  if (action.type === 'REQUEST') {
    return {
      ...state,
      loading: true
    }
  }
  if (action.type === 'SUCCESS') {
    return {
      ...state,
      loading: false,
      data: action.data
    }
  }
  // manipular o estado
  return state
}

function App() {
  const [data, dispatch] = useReducer(reducer, {
    loading: true,
    data: {}
  })

  useEffect(() => {
    dispatch({ type: 'REQUEST' })
    axios.get(url)
      .then(res => {
        dispatch({ type: 'SUCCESS', data: res.data })
      })
  }, [])

  return (
    <div>
      <h1>My Money</h1>
      { JSON.stringify(data) }
      { data.loading && <p>Loading...</p> }
    </div>
  );
}

export default App
```

### Criando hooks personalizados

```
touch useGet.js
```

### Assincronismo, callback e promise

```js
// promise
const setTimeoutPromise = (time, text) => new Promise((resolve, reject) => {
  setTimeout(() => {
  console.log(text)
  resolve()
  }, time)
})

setTimeoutPromise(2000, 'Olá 1')
  .then(() => setTimeoutPromise(1000, 'Olá 2'))
  .then(() => setTimeoutPromise(1000, 'Olá 3'))
```

```js
// async - await
const setTimeoutPromise = (time, text) => new Promise((resolve, reject) => {
  setTimeout(() => {
  console.log(text)
  resolve(text)
  }, time)
})

const func = async() => {
  await setTimeoutPromise(2000, 'Olá 1')
  await setTimeoutPromise(1000, 'Olá 2')
  return 'DevPleno'
}

func()
```

E essa função `async` tem uma `promise`

```js
func().then((retorno) => {
  console.log('Terminou', retorno);
})
```

### Hooks personalizados - usePost

```
touch usePost.js
```


### Hooks personalizados - useDelete

```
cp usePost.js useDelete.js
```

### Organizando os hooks

```
touch rest.js
```

### Bootstrap

```
npm install bootstrap
```

### React Fragments / Componentização (SOC)

```
mkdir elements
touch elements/Headers.js
touch Meses.js
touch AdicionarMes.js
```

```js
#App.js
import React from 'react'
import Rest from './rest'
import Header from './elements/Header'
import Meses from './Meses'
import AdicionarMes from './AdicionarMes'

const baseUrl = 'https://mymoney-rg3915.firebaseio.com/'
const { useGet, usePost, useDelete } = Rest(baseUrl)

function App() {
  // const data = useGet('movimentacoes/2020-10')
  const data = useGet('meses')
  // const [postData, post] = usePost('movimentacoes/2020-10')
  // const [deleteData, remove] = useDelete()

  const saveNew = () => {
    // post({ valor: 10, descricao: 'Lorem ipsum' })
  }

  const doRemove = () => {
    // remove('movimentacoes/2020-10/-MIrvKQBaptAnWzNgUTV')
  }

  return (
    <div>
      <Header />
      <div className="container">
        <AdicionarMes />
        <Meses />
      </div>
    </div>
  );
}

export default App
```


### Organizando a navegação com React-Router-Dom

```
npm install react-router-dom
```

```
mkdir -p pages/Home
touch pages/Movimentacoes.js
touch pages/Home/index.js
mv AdicionarMes.js pages/Home/
mv Meses.js pages/Home/
mkdir utils
mv rest.js utils
mv use*.js utils
```

### Adicionar e remover movimentação / Refetch

Refetch atualiza os dados na tela.


### Hook useRef e criando um novo mês

useRef tem a ver com o DOM e virtualDOM. Tem a ver com as referências.

### Firebase Cloud Functions


```
npm install -g firebase-tools
firebase login
firebase init
firebase deploy --only functions
```

### Atualizando previsões / criando o hook personalizado usePatch


### Deploy

```
npm run build
firebase deploy --only hosting
```

### Autenticação e Autorização

* Autenticação por e-mail e senha
  - identificação temporária
    - sessão - identificador string
    - JWT (JSON web token)

* Autenticação de 2 fatores: SMS, e-mail ou token físico

* Autorização: o que posso fazer?

### JWT e Firebase

No Firebase, escolhemos um
* método de login e
* criamos um usuário

#### Testando no Postman

https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

* Configurações do projeto > Adicionar o Firebase ao seu app da Web

`apiKey: "********"`

jwt.io



### Authorization no Firebase

#### Regras do Firebase

Editar as regras do firebase

{
  "rules": {
    ".read": "auth.provider == 'password'",
    ".write": "auth.provider == 'password'",
  }
}


No Postman você precisa passar também o

{
    "email": "<email>",
    "password": "<password>",
    "returnSecureToken": true
}

Dai você pode acessar

https://mymoney-rg3915.firebaseio.com/meses.json?auth=<token>


* Editar as regras do firebase novamente

{
  "rules": {
    ".read": "auth.provider == 'password'",
    ".write": "auth.uid == '<UID do usuário>'",
  }
}


### Login no My Money

touch my-money/src/pages/Login.js

  const login = async () => {
    const token = await signin({
      email: "***********",
      password: "********",
      returnSecureToken: true
    })
    // imperativa
    console.log('token', token);
  }


### Manipulando erros

### Manipulando erros do Axios

### Criando custom hook (refatoração)

```
cd src
mkdir api
touch api/index.js

mkdir pages/Movimentacoes
mv pages/Movimentacoes.js pages/Movimentacoes/index.js
touch pages/Movimentacoes/InfoMes.js
touch pages/Movimentacoes/AdicionarMovimentacao.js
```

### Corrigindo autenticação




## Projeto: Comentaki


```
create-react-app comentaki
```

Criar um projeto no Firebase


npm install -g firebase-tools


cd comentaki/src
touch firebase.js

Limpar `App.js`

```js
import './App.css';

function App() {
  return (
    <div>
    </div>
  );
}

export default App;
```

### Criando custom hook para carregar dados do Firebase


### Criando um novo item no Firebase

